


Marvin

Principais Funções:

Função run():

    Utilizei o setAhead(200) e o setTurnRight(180) para que ele faça movimentos em círculos e não se tornar um alvo fácil para os oponentes. Também deixei o setTurnGunLeft(15) para auxiliar a função onScannedRobot() no momento do tiro.

Função onScannedRobot():

    Nessa função utilizei o setBack(200), setTurnLeft(180) e setTurnGunRight(15) para fazer o movimento contrário da função run(), desse jeito o robô não fica parado enquanto atira.
    Utilizei também um "if" para que quando a distancia for menor que 300 pixels o poder de tiro vá para 3, além do back(300) que em conjunto com a função run() faz com que o robô dê voltas em torno do inimigo.




