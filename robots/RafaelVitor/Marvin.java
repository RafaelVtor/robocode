package RafaelVitor;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Marvin - a robot by (Rafael Vitor Alves)
 */
public class Marvin extends AdvancedRobot
{
	/**
	 * run: Marvin's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		//setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		boolean x = false;
		// Robot main loop
		while(true) {
		
		
		//	ahead(300);
		//	turnRight(90);
		//	turnGunRight(180);
		//	back(100);
		//	turnRight(90);
		//	turnGunRight(180);

			setAhead(200);
			setTurnRight(180);
			setTurnGunLeft(15);
			
			execute();

			
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		double dist = e.getDistance();
		double anguloArma = getGunHeading();
		
		
		fire(1);
	
		setBack(200);
		setTurnLeft(180);
		setTurnGunRight(15);

	
		if(dist < 300){
			fire(3);
			back(200);
		}

		
		scan();	


		
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		//back(50);
		turnGunRight(e.getBearing());
		
		
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		turnLeft(135);
		back(100);

		
	}	
    public void onHitRobot(HitRobotEvent e){
		
		fire(3);
	}
}
